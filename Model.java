
public class Model {

	//Where the calculations are set up
	
	public int calcModifier(int skill) // Calculates the modifier for a particular stat
	{
		return ((skill-10)/2);
	}
	
	public int calcHealth(int level, int ConMod) // Calculates a character's health stat
	{
		if(ConMod<0)
		{
			ConMod=1;
		}
		return (10+(ConMod*level)+((int)(6*Math.random()+1)*level));
	}
	
	public int calcArmor(int DexMod) // Calculates a character's armor stat
	{
		return (10+DexMod);
	}
	
}
