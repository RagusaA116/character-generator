	import java.awt.event.ActionListener;
	import javax.swing.*;

	public class View extends JFrame
	{
		
			// Creating the Character Sheet's text boxes
		
		private JTextField name  = new JTextField("Player Character",25);
		private JTextField origin  = new JTextField("Player Hometown",25);
		private JTextField Str  = new JTextField("8",4);
		private JTextField StrMod  = new JTextField("-1",2);
		private JTextField Dex  = new JTextField("8",4);
		private JTextField DexMod  = new JTextField("-1",2);
		private JTextField Con  = new JTextField("8",4);
		private JTextField ConMod  = new JTextField("-1",2);
		private JTextField Int  = new JTextField("8",4);
		private JTextField IntMod  = new JTextField("-1",2);
		private JTextField Wis  = new JTextField("8",4);
		private JTextField WisMod  = new JTextField("-1",2);
		private JTextField Cha  = new JTextField("8",4);
		private JTextField ChaMod  = new JTextField("-1",2);
		private JTextField Health = new JTextField("10",3);
		private JTextField Armor = new JTextField("10",3);
		private JTextField Lvl = new JTextField("1",2);

		
			// Creating the menu labels
		
		private JLabel NameLabel = new JLabel("Name:");
		private JLabel OriginLabel = new JLabel("Origin:");
		private JLabel Strength = new JLabel("Strength:       ");
		private JLabel Dexterity = new JLabel("Dexterity:      ");
		private JLabel Constitution = new JLabel("Constitution:");
		private JLabel Intelligence = new JLabel("Intelligence:");
		private JLabel Wisdom = new JLabel("Wisdom:      ");
		private JLabel Charisma = new JLabel("Charisma:   ");
		private JLabel Level = new JLabel("Level:                            ");
		private JLabel HP = new JLabel("Hitpoints:                       ");
		private JLabel AC = new JLabel("Armor Class:                   ");
		
			// Interface buttons
		
	    private JButton RandomName = new JButton("Randomize Name");
	    private JButton RandomOrigin = new JButton("Randomize Origin");
	    private JButton ConfirmStats = new JButton("Confirm Stats");
	    private JButton RandomizeStats = new JButton("Randomize Stats");
	    private JButton Save = new JButton("Save");
	    private JButton Load = new JButton("Load");
		
			// Create spacing so that the menu isn't shoved all together
	    
	    private JLabel StrSpace = new JLabel("                                                                ");

	    private JLabel DexSpace = new JLabel("                                                                 ");
	    private JLabel lvlbox = new JLabel("                            ");

	    private JLabel ConSpace = new JLabel("                                                               ");

	    private JLabel IntSpace = new JLabel("                                                                 ");
	    private JLabel healthbox = new JLabel("                        ");

	    private JLabel WisSpace = new JLabel("                                                            ");

	    private JLabel ChaSpace = new JLabel("                                                                 ");
	    private JLabel armorbox = new JLabel("                         ");
	    
		private JLabel CSspace = new JLabel("                                                                                                                        ");
		private JLabel RSspace = new JLabel("   ");

		private JLabel LoadSpace = new JLabel("   ");
		
	    View(){
		    
	        JPanel CharacterSheet = new JPanel();
	        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        this.setSize(500, 300);

	        CharacterSheet.add(NameLabel);
	        CharacterSheet.add(name);
	        CharacterSheet.add(RandomName);

	        CharacterSheet.add(OriginLabel);
	        CharacterSheet.add(origin);
	        CharacterSheet.add(RandomOrigin);
	        
	        CharacterSheet.add(Strength);
	        CharacterSheet.add(Str);
	        CharacterSheet.add(StrMod);
	        CharacterSheet.add(StrSpace);
	        CharacterSheet.add(Level);

	        CharacterSheet.add(Dexterity);
	        CharacterSheet.add(Dex);
	        CharacterSheet.add(DexMod);
	        CharacterSheet.add(DexSpace);
	        CharacterSheet.add(Lvl);
	        CharacterSheet.add(lvlbox);

	        CharacterSheet.add(Constitution);
	        CharacterSheet.add(Con);
	        CharacterSheet.add(ConMod);
	        CharacterSheet.add(ConSpace);
	        CharacterSheet.add(HP);
	        
	        CharacterSheet.add(Intelligence);
	        CharacterSheet.add(Int);
	        CharacterSheet.add(IntMod);
	        CharacterSheet.add(IntSpace);
	        CharacterSheet.add(Health);
	        CharacterSheet.add(healthbox);
	        
	        CharacterSheet.add(Wisdom);
	        CharacterSheet.add(Wis);
	        CharacterSheet.add(WisMod);
	        CharacterSheet.add(WisSpace);
	        CharacterSheet.add(AC);
	        
	        CharacterSheet.add(Charisma);
	        CharacterSheet.add(Cha);
	        CharacterSheet.add(ChaMod);
	        CharacterSheet.add(ChaSpace);
	        CharacterSheet.add(Armor);
	        CharacterSheet.add(armorbox);
	        
	        CharacterSheet.add(ConfirmStats);
	        CharacterSheet.add(RandomizeStats);
	        CharacterSheet.add(RSspace);
	        
	        CharacterSheet.add(Save);
	        CharacterSheet.add(Load);
	        CharacterSheet.add(LoadSpace);
	        
	        this.add(CharacterSheet);
	    }
	    
	    	// Obtain a certain entry from the menu
	    
	    public String getName()
	    {
	    	return (name.getText());
	    }
	    
	    public String getOrigin()
	    {
	    	return (origin.getText());
	    }
	    
	    public int getStr()
	    {
	    	return (Integer.parseInt(Str.getText()));
	    }
	    
	    public int getDex()
	    {
	    	return (Integer.parseInt(Dex.getText()));
	    }
	    
	    public int getCon()
	    {
	    	return (Integer.parseInt(Con.getText()));
	    }
	    
	    public int getWis()
	    {
	    	return (Integer.parseInt(Wis.getText()));
	    }
	    
	    public int getInt()
	    {
	    	return (Integer.parseInt(Int.getText()));
	    }
	    
	    public int getCha()
	    {
	    	return (Integer.parseInt(Cha.getText()));
	    }
	    
	    public int getLevel()
	    {
	    	return (Integer.parseInt(Lvl.getText()));
	    }
	    
	    public int getHealth()
	    {
	    	return (Integer.parseInt(Health.getText()));
	    }

	    public int getArmor()
	    {
	    	return (Integer.parseInt(Armor.getText()));
	    }
	  
	    	// Change an entry from the menu
	    
	    public void setStr(int skill)
	    {
	    	Str.setText(Integer.toString(skill));
	    }
	    
	    public void setDex(int skill)
	    {
	    	Dex.setText(Integer.toString(skill));
	    }
	    
	    public void setCon(int skill)
	    {
	    	Con.setText(Integer.toString(skill));
	    }
	    
	    public void setInt(int skill)
	    {
	    	Int.setText(Integer.toString(skill));
	    }
	    
	    public void setWis(int skill)
	    {
	    	Wis.setText(Integer.toString(skill));
	    }
	    
	    public void setCha(int skill)
	    {
	    	Cha.setText(Integer.toString(skill));
	    }

	    public void setName(String Name)
	    {
	    	name.setText(Name);
	    }
	    
	    public void setPlace(String place)
	    {
	    	origin.setText(place);
	    }
	    
	    public void setStrMod(int mod)
	    {
	    	StrMod.setText(Integer.toString(mod));
	    }
	    
	    public void setDexMod(int mod)
	    {
	    	DexMod.setText(Integer.toString(mod));
	    }
	    
	    public void setConMod(int mod)
	    {
	    	ConMod.setText(Integer.toString(mod));
	    }
	    
	    public void setWisMod(int mod)
	    {
	    	WisMod.setText(Integer.toString(mod));
	    }
	    
	    public void setIntMod(int mod)
	    {
	    	IntMod.setText(Integer.toString(mod));
	    }
	    
	    public void setChaMod(int mod)
	    {
	    	ChaMod.setText(Integer.toString(mod));
	    }
	    
	    public void setLevel(int newLevel)
	    {
	    	Lvl.setText(Integer.toString(newLevel));
	    }
	    
	    public void setHealth(int newHealth)
	    {
	    	Health.setText(Integer.toString(newHealth));
	    }
	    
	    public void setArmor(int newArmor)
	    {
	    	Armor.setText(Integer.toString(newArmor));
	    }
	    
	    	// Gives functionality to the menu buttons
	    
	    void ConfirmStats(ActionListener calcStats)  
	    {
	    	ConfirmStats.addActionListener(calcStats);
	    }
	    
	    void RandomName(ActionListener RandomNameGen)
	    {
	    	RandomName.addActionListener(RandomNameGen);
	    }
	    
	    void RandomOrigin(ActionListener RandomPlace)
	    {
	    	RandomOrigin.addActionListener(RandomPlace);
	    }
	    
	    void RandomizeStats(ActionListener RanStats)
	    {
	    	RandomizeStats.addActionListener(RanStats);
	    }
	    
	    void Save(ActionListener SaveCharacter)
	    {
	    	Save.addActionListener(SaveCharacter);
	    }
	    
	    void Load(ActionListener LoadCharacter)
	    {
	    	Load.addActionListener(LoadCharacter);
	    }

	    	// Open a popup that contains the error message passed

	    void displayErrorMessage(String errorMessage)
	    {
	        JOptionPane.showMessageDialog(this, errorMessage);
	    }
	}
