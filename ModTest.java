import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ModTest {

	@Test
	public void testMod() {
			Model test = new Model();
			int output = test.calcModifier(8);
			assertEquals(-1, output);
	}
}
