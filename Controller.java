import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;
import java.io.FileNotFoundException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

// The Controller coordinates interactions between the View and Model

public class Controller
{
    private View theView;
    private Model theModel;
    public Controller(View theView, Model theModel)
    {

        this.theView = theView;
        this.theModel = theModel;

        playSound(); //PLAYS MUSIC
        
        this.theView.ConfirmStats(new CheckStats());
        this.theView.RandomName(new RandomNameGen());
        this.theView.RandomOrigin(new RandomPlace());
        this.theView.RandomizeStats(new RanStats());
        this.theView.Save(new SaveCharacter());
        this.theView.Load(new LoadCharacter());
        
    }

    class CheckStats implements ActionListener // Confirms the current menu's stats
    {
    	public void actionPerformed (ActionEvent e)
    	{
    		try
    		{
            		theView.setStrMod(theModel.calcModifier(theView.getStr()));
            
            		theView.setDexMod(theModel.calcModifier(theView.getDex()));
           
            		theView.setConMod(theModel.calcModifier(theView.getCon()));
            
            		theView.setIntMod(theModel.calcModifier(theView.getInt()));
            		
            		theView.setWisMod(theModel.calcModifier(theView.getWis()));

            		theView.setChaMod(theModel.calcModifier(theView.getCha()));
            	
            		theView.setHealth(theModel.calcHealth(theView.getLevel(),theModel.calcModifier(theView.getCon())));
            		
            		theView.setArmor(theModel.calcArmor(theModel.calcModifier(theView.getDex())));
    		}
    		catch(NumberFormatException ex)
    		{
    			System.out.println(ex);

                theView.displayErrorMessage("Stats must be entered as integers");
    		}
    	}
    }
    
    class RandomNameGen implements ActionListener // Generates a random name from an ArrayList
    {
    	public void actionPerformed (ActionEvent b)
    	{
    			ArrayList<String> names = new ArrayList<String>();
    			names.add("Marvin");
    			names.add("Vaki");
    			names.add("Anton");
    			names.add("Zan'thas");
    			names.add("Alana");
    			names.add("Romeo");
    			names.add("Miguel");
    			names.add("Yuna");
    			names.add("Dax");
    			names.add("Brandy");
    			names.add("The Nowhere Man");
    			names.add("Dilbert");
    			names.add("Dar'Talon");
    			
    			int num = (int)((names.size())*Math.random());
    			
    			theView.setName(names.get(num));
    	}
    }	
   
    class RandomPlace implements ActionListener // Generates a random place from an ArrayList
    {
    	public void actionPerformed (ActionEvent c)
    	{
    		ArrayList<String> place = new ArrayList<String>();
    		place.add("Lumbridge");
    		place.add("Varrock");
    		place.add("Falador");
    		place.add("Camelot");
    		place.add("The Wilderness");
    		place.add("Yanille");
    		place.add("Al Kharid");
    		place.add("Ardougne");
    		place.add("Prifddinas");
    		
    		int num = (int)((place.size())*Math.random());
    		
    		theView.setPlace(place.get(num));
    	}
    }
    	
    class RanStats implements ActionListener // Generates random stats from a range of 1-20, then confirms the stats
    {
    	public void actionPerformed (ActionEvent d)
    	{
    		theView.setStr((int)((20)*Math.random()+1));
    		theView.setStrMod((theModel.calcModifier(theView.getStr())));
    		
    		theView.setDex((int)((20)*Math.random()+1));
    		theView.setDexMod((theModel.calcModifier(theView.getDex())));
    		
    		theView.setCon((int)((20)*Math.random()+1));
    		theView.setConMod((theModel.calcModifier(theView.getCon())));
    		
    		theView.setInt((int)((20)*Math.random()+1));
    		theView.setIntMod((theModel.calcModifier(theView.getInt())));
    		
    		theView.setWis((int)((20)*Math.random()+1));
    		theView.setWisMod((theModel.calcModifier(theView.getWis())));
    		
    		theView.setCha((int)((20)*Math.random()+1));
    		theView.setChaMod((theModel.calcModifier(theView.getCha())));

    		theView.setLevel((int)((20)*Math.random()+1));
    		
    		theView.setHealth((theModel.calcHealth(theView.getLevel(),theModel.calcModifier(theView.getCon()))));
    		
    		theView.setArmor((theModel.calcArmor(theModel.calcModifier(theView.getDex()))));
    	}
    }
    
    class SaveCharacter implements ActionListener // Saves the current character to directory
    {
    	public void actionPerformed (ActionEvent f)
    	{
    		//output
    		PrintWriter outputStream=null;
    		try
    		{
    			outputStream = new PrintWriter(theView.getName()+".txt");

    			outputStream.println(theView.getName());
    			outputStream.println(theView.getOrigin());
    			outputStream.println(theView.getStr());
    			outputStream.println(theView.getDex());
    			outputStream.println(theView.getCon());
    			outputStream.println(theView.getInt());
    			outputStream.println(theView.getWis());
    			outputStream.println(theView.getCha());
    			outputStream.println(theView.getLevel());
    			outputStream.println(theView.getHealth());
    			outputStream.println(theView.getArmor());
    		
    			outputStream.close();
    		}
    		catch(FileNotFoundException e)
    		{
    			theView.displayErrorMessage("File not found :(");
    		}
    	}
    }
    
    class LoadCharacter implements ActionListener // Loads a character from directory
    {
    	public void actionPerformed (ActionEvent g)
    	{
    		fileChooser();

    		theView.setStrMod(theModel.calcModifier(theView.getStr()));
            
    		theView.setDexMod(theModel.calcModifier(theView.getDex()));
   
    		theView.setConMod(theModel.calcModifier(theView.getCon()));
    
    		theView.setIntMod(theModel.calcModifier(theView.getInt()));
    		
    		theView.setWisMod(theModel.calcModifier(theView.getWis()));

    		theView.setChaMod(theModel.calcModifier(theView.getCha()));
    	    		
    		theView.setArmor(theModel.calcArmor(theModel.calcModifier(theView.getDex())));
    	}
    }
    
    public void playSound() // Plays background music
    {
        try
        {
        	AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("Old RuneScape Soundtrack Newbie Melody.wav").getAbsoluteFile());
        	Clip clip = AudioSystem.getClip();
        	clip.open(audioIn);
        	clip.start();
        	clip.loop(Clip.LOOP_CONTINUOUSLY);
        }
        
        catch(Exception ex)
        {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    
    public void fileChooser() // Allows for a file from directory to be chosen
    {
    	JFileChooser menu = new JFileChooser();
    
    		menu.setCurrentDirectory(new java.io.File("."));
    		menu.setDialogTitle("Load Character");
    		menu.setFileSelectionMode(JFileChooser.FILES_ONLY);
    		FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt");
    		menu.setFileFilter(filter);
    		
    		if (menu.showOpenDialog(menu) == JFileChooser.APPROVE_OPTION)
    		{
    			File file = menu.getSelectedFile();
    			try
    			{
					Scanner input = new Scanner(file);
					if(file!=null)
					{
						theView.setName(input.nextLine());
						theView.setPlace(input.nextLine());
						theView.setStr(Integer.parseInt(input.nextLine()));
						theView.setDex(Integer.parseInt(input.nextLine()));
						theView.setCon(Integer.parseInt(input.nextLine()));
						theView.setInt(Integer.parseInt(input.nextLine()));
						theView.setWis(Integer.parseInt(input.nextLine()));
						theView.setCha(Integer.parseInt(input.nextLine()));
						theView.setLevel(Integer.parseInt(input.nextLine()));
						theView.setHealth(Integer.parseInt(input.nextLine()));
						theView.setArmor(Integer.parseInt(input.nextLine()));
					}
					else
					{
		                theView.displayErrorMessage("Error with file contents");
					}
					input.close();
    			}
    			catch(FileNotFoundException e)
    			{
    				e.printStackTrace();
	                theView.displayErrorMessage("Error with file");
    			}
    		}
    }
}
