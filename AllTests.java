import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

@RunWith(Suite.class)
@SuiteClasses({})
public class AllTests {

		@Test
		public void testMod() {
				Model test = new Model();
				int output = test.calcModifier(8);
				assertEquals(-1, output);
		}
	
		public void testArmor() {
			Model test = new Model();
			int output = test.calcArmor(-1);
			assertEquals(-1, output);
		}
	}

