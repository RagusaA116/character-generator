package Character;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ArmorTest {

	@Test
		public void testArmor() {
			Model test = new Model();
			int output = test.calcArmor(-1);
			assertEquals(9, output);
	}

}
